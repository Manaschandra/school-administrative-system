package com.sas.schooladministrativesystem;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import static java.lang.Math.toIntExact;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

/**
 *
 * @author Manas
 */
@RunWith(Parameterized.class)
public class SubjectsTest {
    private Subjects subject;
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        subject = new Subjects();
    }
    
    @After
    public void tearDown() {
    }
    
    //loading test data from an external JSON file.
    @Parameterized.Parameters
    public static Collection testData1() {
        List<Object[]> result = new ArrayList<>();
        JSONParser parser = new JSONParser();
        try{
        Object obj = parser.parse(new FileReader(new File("testData.json")));
        JSONObject jsonobj = (JSONObject) obj;
        JSONArray studentData = (JSONArray) jsonobj.get("Students");
        for(int temp = 0; temp < studentData.size(); temp++) {
            JSONObject jsonTempObj = (JSONObject) studentData.get(temp);
            JSONArray tempArray = (JSONArray) jsonTempObj.get("individualSubjects");
            String[] tempStrArray = new String[tempArray.size()];
            for(int i = 0; i < tempArray.size(); i++) {
                tempStrArray[i] = tempArray.get(i).toString();
            }
            result.add(new Object[] {(String) jsonTempObj.get("personalNumber"),(String) jsonTempObj.get("name"),toIntExact((long) jsonTempObj.get("grade")),tempStrArray});
        }
        }
        catch (IOException e){
            e.printStackTrace();
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
       return result;
    }
    
    private String studentPersonalNumber;
    private String studentName;
    private int studentGrade;
    private String[] studentIndividualSubjects;
    
    public SubjectsTest(String studentPersonalNumber, String studentName, int studentGrade, String[] studentIndividualSubjects) {
        this.studentPersonalNumber = studentPersonalNumber;
        this.studentName = studentName;
        this.studentGrade = studentGrade;
        this.studentIndividualSubjects = studentIndividualSubjects;
    }

    //Test of getCommonSubjects method, of class Subjects.
    @Test
    public void testGetCommonSubjects() {
        //System.out.println("getCommonSubjects");
        String[] testArray = subject.getCommonSubjects(studentGrade);
        switch(studentGrade) {
            case 1: assertArrayEquals(new String[] {"Swedish", "English", "Math"}, testArray);
                break;
            case 2: assertArrayEquals(new String[] {"Swedish", "English", "Math", "Physics"}, testArray);
                break;
            case 3: assertArrayEquals(new String[] {"Swedish", "English", "Math", "Physics", "Geography"}, testArray);
                break;
            case 4: assertArrayEquals(new String[] {"Swedish", "English", "Math", "Physics", "Geography", "Chemistry"}, testArray);
                break;
        }
    }

    // Test of getAllSubjects method, of class Subjects.
    @Test
    public void testGetAllSubjects() {
        //System.out.println("getAllSubjects");
        String[] expectedArray = {"Swedish", "English", "Math", "Physics", "Geography", "Chemistry", "Danish", "French", "Programming"};
        String[] testArray = subject.getAllSubjects();
        assertArrayEquals("9 subjects",expectedArray, testArray);
    }

    // Test of getTotalNumberOfSubjects method, of class Subjects.
    @Test
    public void testGetTotalNumberOfSubjects() {
        //System.out.println("getTotalNumberOfSubjects");
        int expResult = 9;
        int result = subject.getTotalNumberOfSubjects();
        assertEquals(expResult, result);
    } 
}
