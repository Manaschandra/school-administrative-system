package com.sas.schooladministrativesystem;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import static java.lang.Math.toIntExact;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Stream;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

/**
 *
 * @author Manas
 */
@RunWith(Parameterized.class)
public class StudentsTest {
    
    private Students student;
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    //loading test data from an external JSON file.
    @Parameterized.Parameters
    public static Collection testData1() {
        List<Object[]> result = new ArrayList<>();
        JSONParser parser = new JSONParser();
        try{
        Object obj = parser.parse(new FileReader(new File("testData.json")));
        JSONObject jsonobj = (JSONObject) obj;
        JSONArray studentData = (JSONArray) jsonobj.get("Students");
        for(int temp = 0; temp < studentData.size(); temp++) {
            JSONObject jsonTempObj = (JSONObject) studentData.get(temp);
            JSONArray tempArray = (JSONArray) jsonTempObj.get("individualSubjects");
            String[] tempStrArray = new String[tempArray.size()];
            for(int i = 0; i < tempArray.size(); i++) {
                tempStrArray[i] = tempArray.get(i).toString();
            }
            result.add(new Object[] {(String) jsonTempObj.get("personalNumber"),(String) jsonTempObj.get("name"),(String) jsonTempObj.get("address"),toIntExact((long) jsonTempObj.get("grade")),tempStrArray});
        }
        }
        catch (IOException e){
            e.printStackTrace();
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
       return result;
    }
    
    private String studentPersonalNumber;
    private String studentName;
    private String address;
    private int studentGrade;
    private String[] studentIndividualSubjects;
    
    public StudentsTest(String studentPersonalNumber, String studentName, String address, int studentGrade, String[] studentIndividualSubjects) {
        this.studentPersonalNumber = studentPersonalNumber;
        this.studentName = studentName;
        this.address = address;
        this.studentGrade = studentGrade;
        this.studentIndividualSubjects = studentIndividualSubjects;
        student = new Students(studentPersonalNumber, studentName, address, studentGrade, studentIndividualSubjects);
    }
    
    //Test of getCompleteSubjects method, of class Students.
    @Test
    public void testGetCompleteSubjects() {
        //System.out.println("getCompleteSubjects");
        Subjects subjects = new Subjects();
        String[] expResult = Stream.concat(Arrays.stream(subjects.getCommonSubjects(studentGrade)), Arrays.stream(studentIndividualSubjects))
                      .toArray(String[]::new);
        String[] result = student.getCompleteSubjects();
        assertArrayEquals(expResult, result);
    }

    //Test of getIndividualStubjects method, of class Students.
    @Test
    public void testGetIndividualStubjects() {
        //System.out.println("getIndividualStubjects");
        String[] expResult = studentIndividualSubjects;
        String[] result = student.getIndividualStubjects();
        assertArrayEquals(expResult, result);;
    }

    //Test of setClassOfStudent method, of class Students.
    @Test
    public void testSetClassOfStudent() {
        //System.out.println("setClassOfStudent");
    }

    //Test of getClassOfStudent method, of class Students.
    @Test
    public void testGetClassOfStudent() {
        //System.out.println("getClassOfStudent");
    }

    //Test of getGradeOfStudent method, of class Students.
    @Test
    public void testGetGradeOfStudent() {
        //System.out.println("getGradeOfStudent");
        int expResult = studentGrade;
        int result = student.getGradeOfStudent();
        assertEquals(expResult, result);
    }

    // Test of getPersonalNumber method, of class Students.
    @Test
    public void testGetPersonalNumber() {
        //System.out.println("getPersonalNumber");;
        String expResult = studentPersonalNumber;
        String result = student.getPersonalNumber();
        assertEquals(expResult, result);
    }
    
}
