package com.sas.schooladministrativesystem;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import static java.lang.Math.toIntExact;
import java.util.ArrayList;
import java.util.List;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Manas
 */
public class SASTest {
    
    private Students student;
    
    public SASTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    //loading test data from an external JSON file.
    public static List testData1() {
        List<Students> students = new ArrayList<Students>();
        JSONParser parser = new JSONParser();
        try{
        Object obj = parser.parse(new FileReader(new File("testData.json")));
        JSONObject jsonobj = (JSONObject) obj;
        JSONArray studentData = (JSONArray) jsonobj.get("Students");
        for(int temp = 0; temp < studentData.size(); temp++) {
            JSONObject jsonTempObj = (JSONObject) studentData.get(temp);
            JSONArray tempArray = (JSONArray) jsonTempObj.get("individualSubjects");
            String[] tempStrArray = new String[tempArray.size()];
            for(int i = 0; i < tempArray.size(); i++) {
                tempStrArray[i] = tempArray.get(i).toString();
            }
            students.add(new Students((String) jsonTempObj.get("personalNumber"), (String) jsonTempObj.get("name"),(String) jsonTempObj.get("address"), toIntExact((long) jsonTempObj.get("grade")),tempStrArray));
        }
        }
        catch (IOException e){
            e.printStackTrace();
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
       return students;
    }
    
    //Test of main method, of class SAS.
    @Test
    public void testMain() {
        System.out.println("main");
        SAS sas = new SAS(testData1());
        String[] arg = null;
        sas.main(arg);
    }
    
}
