package com.sas.schooladministrativesystem;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import static java.lang.Math.toIntExact;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

/**
 *
 * @author Manas
 */
@RunWith(Parameterized.class)
public class GroupsTest {
    
    private Groups group;
    private Students student;
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    
    //loading test data from an external JSON file.
    @Parameterized.Parameters
    public static Collection testData1() {
        List<Object[]> result = new ArrayList<>();
        JSONParser parser = new JSONParser();
        try{
        Object obj = parser.parse(new FileReader(new File("testData.json")));
        JSONObject jsonobj = (JSONObject) obj;
        JSONArray studentData = (JSONArray) jsonobj.get("Students");
        for(int temp = 0; temp < studentData.size(); temp++) {
            JSONObject jsonTempObj = (JSONObject) studentData.get(temp);
            JSONArray tempArray = (JSONArray) jsonTempObj.get("individualSubjects");
            String[] tempStrArray = new String[tempArray.size()];
            for(int i = 0; i < tempArray.size(); i++) {
                tempStrArray[i] = tempArray.get(i).toString();
            }
            result.add(new Object[] {(String) jsonTempObj.get("personalNumber"),(String) jsonTempObj.get("name"),(String) jsonTempObj.get("address"),toIntExact((long) jsonTempObj.get("grade")),tempStrArray});
        }
        }
        catch (IOException e){
            e.printStackTrace();
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
       return result;
    }
    
    private String studentPersonalNumber;
    private String studentName;
    private String address;
    private int studentGrade;
    private String[] studentIndividualSubjects;
    
    public GroupsTest(String studentPersonalNumber, String studentName, String address, int studentGrade, String[] studentIndividualSubjects) {
        this.studentPersonalNumber = studentPersonalNumber;
        this.studentName = studentName;
        this.address = address;
        this.studentGrade = studentGrade;
        this.studentIndividualSubjects = studentIndividualSubjects;
        student = new Students(studentPersonalNumber, studentName, address, studentGrade, studentIndividualSubjects);
    }

    // Test of addToGroup method, of class Groups.
    @Test
    public void testAddToGroup() {
        //System.out.println("addToGroup");
        Subjects subjects = new Subjects();
        group = new Groups(subjects.getCommonSubjects(studentGrade), studentGrade);
        boolean expResult = true;
        boolean result = group.addToGroup(student);;
        assertEquals(expResult, result);
    }

    // Test of getSubjects method, of class Groups.
    @Test
    public void testGetSubjects() {
        //System.out.println("getSubjects");
        Subjects subjects = new Subjects();
        group = new Groups(subjects.getCommonSubjects(studentGrade), studentGrade);
        group.addToGroup(student);
        String[] expResult = subjects.getCommonSubjects(studentGrade);
        String[] result = group.getSubjects();
        assertArrayEquals(expResult, result);
    }

    //Test of getSubject method, of class Groups.
    @Test
    public void testGetSubject() {
        //System.out.println("getSubject");
        Subjects subjects = new Subjects();
        group = new Groups(subjects.getCommonSubjects(studentGrade), studentGrade);
        group.addToGroup(student);
        String expResult = student.getCompleteSubjects()[0];
        String result = group.getSubject();
        assertEquals(expResult, result);
    }

    // Test of getStudentsInGroup method, of class Groups.
    @Test
    public void testGetStudentsInGroup() {
        //System.out.println("getStudentsInGroup");
        Subjects subjects = new Subjects();
        group = new Groups(subjects.getCommonSubjects(studentGrade), studentGrade);
        group.addToGroup(student);
        List<Students> expResult = new ArrayList<Students>();
        expResult.add(student);
        List result = group.getStudentsInGroup();
        assertEquals(expResult, result);
    }
    
    //Test of getNumberOfStudentsInGroup method, of class Groups.
    @Test
    public void testGetNumberOfStudentsInGroup() {
        //System.out.println("getNumberOfStudentsInGroup");
        Subjects subjects = new Subjects();
        group = new Groups(subjects.getCommonSubjects(studentGrade), studentGrade);
        group.addToGroup(student);
        int expResult = 1;
        int result = group.getNumberOfStudentsInGroup();
        assertEquals(expResult, result);
    }
    
    //Test of unPlacedStudents method, of class Groups.
    @Test
    public void testUnPlacedStudents() {
        //System.out.println("unPlacedStudents");
    }  

    //Test of showGroup method, of class Groups.
    @Test
    public void testShowGroup() {
        //System.out.println("showGroup");
    }  
}
