package com.sas.schooladministrativesystem;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Manas
 */
public class ClassesTest {
    private Classes testClass;
    public ClassesTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        testClass = new Classes();
    }
    
    @After
    public void tearDown() {
    }
    
    //Test of setClass method, of class Classes.
    @Test
    public void testSetClass() {
        //System.out.println("getTotalNumberOfSubjects");
        String expResult = "1";
        String result = testClass.setClass(1, 20, 5);
        assertEquals(expResult, result);
    }
    
    //Test of setClass method, of class Classes.
    @Test
    public void testSetClass2() {
        //System.out.println("getTotalNumberOfSubjects");
        String expResult = "1A";
        String result = testClass.setClass(1, 25, 5);
        assertEquals(expResult, result);
    }
}
