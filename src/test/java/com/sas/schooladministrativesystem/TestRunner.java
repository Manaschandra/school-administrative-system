package com.sas.schooladministrativesystem;

import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

/**
 *
 * @author Manas
 */
public class TestRunner {
   public static void main(String[] args) {
       //test runner for running all the test classes.
      Result result = JUnitCore.runClasses(SchooladministrativesystemSuite.class);
      for (Failure failure : result.getFailures()) {
         System.out.println(failure.toString());
      }
      System.out.println(result.getRunCount()+ " tests - " +result.wasSuccessful());
   }
}
