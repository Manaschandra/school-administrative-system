package com.sas.schooladministrativesystem;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 *
 * @author Manas
 */
public class Subjects {
    
    //Fixed data so, static data is used.
    final private String[] commonSubjectsGrade1 = {"Swedish", "English", "Math"};
    final private String[] commonSubjectsGrade2 = {"Swedish", "English", "Math", "Physics"};
    final private String[] commonSubjectsGrade3 = {"Swedish", "English", "Math", "Physics", "Geography"};
    final private String[] commonSubjectsGrade4 = {"Swedish", "English", "Math", "Physics", "Geography", "Chemistry"};
    final private String[] individualSubjects = {"Danish", "French", "Programming"};
    final private int numberOfGrades = 4;
    private String[] allSubjects = null;
    private int count = 0;
    
    //constructor to set all the subjects that are available in school.
    public Subjects() {
        Set<String> tempList = new LinkedHashSet<String>();
        tempList.addAll(Arrays.asList(commonSubjectsGrade1));
        tempList.addAll(Arrays.asList(commonSubjectsGrade2));
        tempList.addAll(Arrays.asList(commonSubjectsGrade3));
        tempList.addAll(Arrays.asList(commonSubjectsGrade4));
        tempList.addAll(Arrays.asList(individualSubjects));
        allSubjects = tempList.toArray(new String[tempList.size()]);
        count = allSubjects.length;
    }
    
    //gets the common subjects of the students within same grade.
    public String[] getCommonSubjects(int grade) {
        if (grade <= numberOfGrades && grade > 0){
            switch(grade){
                case 1: return commonSubjectsGrade1;
                case 2: return commonSubjectsGrade2;
                case 3: return commonSubjectsGrade3;
                case 4: return commonSubjectsGrade4;
            }
        }
        
        return null;
    }
    
    //gets all the subjects that are offered by the school.
    public String[] getAllSubjects() {
        return allSubjects;
    }
    
    //gets the total number of subjects that are offered by the school.
    public int getTotalNumberOfSubjects(){
        return count;
    }
}
