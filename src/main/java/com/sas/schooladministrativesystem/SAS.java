package com.sas.schooladministrativesystem;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Manas
 */
public class SAS {
    final private static int totalGrades = 4;
    private static int[] totalNumberOfStudents = new int[totalGrades];
    private static int[] indexOfStudentInQueue = new int[totalGrades];
    private static int groupNumber = 0;
    private static int totalNumberOfGroups = 0;
    static List<Students> student = new ArrayList<Students>();
    
    public SAS(List<Students> students) {
        this.student = students;
    }
    
    public static void main(String arg[]) {
        Classes classes = new Classes();
        Subjects subjects = new Subjects();
        Students[] students = new Students[student.size()];
        
        for(int i = 0; i < student.size(); i++) {
            students[i] = student.get(i);
        }
        
        //total number of students in each grade
        for(Students student : students) {
            totalNumberOfStudents[student.getGradeOfStudent() - 1]++;
        }
        
        List<Groups> groups = new ArrayList<Groups>();
        String[] allSubjects = subjects.getAllSubjects();

        for(Students student : students) {
            int grade = student.getGradeOfStudent();
            
            //index of student who is next in queue.
            indexOfStudentInQueue[grade - 1]++;
            
            //set class for the students.
            student.setClassOfStudent(classes.setClass(grade, totalNumberOfStudents[grade - 1], indexOfStudentInQueue[grade - 1]));
            String[] subjectsStudentStudies = student.getCompleteSubjects();
            
            boolean commonSubjectsFlag = false;
            boolean individualSubjectsFlag = false;
            List<String> remainingSubjects = new ArrayList<String>();
            
            //set groups for the common subjects
            for(groupNumber = 0; groupNumber < totalNumberOfGroups; groupNumber++) {
                if(groups.get(groupNumber).getSubjects() == subjects.getCommonSubjects(grade)) {
                    if(groups.get(groupNumber).addToGroup(student)) {
                        commonSubjectsFlag = true;
                        break;
                    }
                }
            }
            if(commonSubjectsFlag == false) {
                totalNumberOfGroups++;
                groups.add(new Groups(subjects.getCommonSubjects(grade), totalNumberOfGroups));
                groups.get(totalNumberOfGroups - 1).addToGroup(student);
            }
            
            //set groups for the individual subjects
            for(String sub : student.getIndividualStubjects()) {
                boolean flag = false;
                for(groupNumber = 0; groupNumber < totalNumberOfGroups; groupNumber++) {
                    if(groups.get(groupNumber).getSubject().equals(sub)) {
                        if(groups.get(groupNumber).addToGroup(student)) {
                            flag = true;
                        }
                    }
                }
                if(flag == false) 
                    remainingSubjects.add(sub);
                if(remainingSubjects.size() == 0) 
                    individualSubjectsFlag = true;
            }
            if(individualSubjectsFlag == false) {
                for(String sub : remainingSubjects) {
                    totalNumberOfGroups++;
                    groups.add(new Groups(new String[] {sub}, totalNumberOfGroups));
                    groups.get(totalNumberOfGroups - 1).addToGroup(student);
                }
            }
        }
        
        //display the details of all the groups.
        for(Groups group : groups) {
            if(group.reportToHeadMaster() == false)
                group.showGroup();
        }
        
        //display the details of unplaced students.
        for(Groups group : groups) {
            if(group.reportToHeadMaster() == true)
                group.unPlacedStudents();
        }
    }
}
