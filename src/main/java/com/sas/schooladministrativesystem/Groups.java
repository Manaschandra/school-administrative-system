package com.sas.schooladministrativesystem;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Manas
 */
public class Groups {
    private String[] subjects = null;
    private int totalNumberOfStudents = 0;
    private List<Students> students = new ArrayList<Students>();
    private int groupNumber = 0;
    final private int maxStudents = 20;
    final private int minStudents = 5;
    
    public Groups(String[] subjects, int groupNumber) {
        this.subjects = subjects;
        this.groupNumber = groupNumber;
    }
    
    //adding students to the groups, based on subjects they are supposed to study.
    public boolean addToGroup(Students student) {
        if (totalNumberOfStudents < maxStudents) {
            students.add(student);
            totalNumberOfStudents++;
            return true;
        }
        else
            return false;
    }
    
    //gets the common subjects that the students in that particular group are studying.
    public String[] getSubjects() {
        return subjects;
    }
    
    //gets the individual subjects that the students in that particular group are studying.
    public String getSubject() {
        return subjects[0];
    }
    
    //gets the detials of all the students in that particular group.
    public List getStudentsInGroup() {
        return students;
    }
    
    //gets the number of students in the group.
    public int getNumberOfStudentsInGroup() {
        return totalNumberOfStudents;
    }
    
    //report to head master.
    public boolean reportToHeadMaster() {
        if(totalNumberOfStudents < minStudents)
            return true;
        else
            return false;
    }
    
    //displays all the details of the unplaced students.
    public void unPlacedStudents() {
        System.out.println("\nGroup-" + groupNumber + " contains " + totalNumberOfStudents + " unplaced students." );
        System.out.println("Subjects:");
        for(String sub : subjects) {
            System.out.println("\t" + sub);
        }
        System.out.println("Personal-Number  Class");
        for(int i = 0; i < students.size(); i++) {
            System.out.println(students.get(i).getPersonalNumber() + " \t " + students.get(i).getClassOfStudent());
        }
    }
    
    //displays all the details of the group.
    public void showGroup() {
        System.out.println("\nGroup-" + groupNumber + " contains " + totalNumberOfStudents + " students." );
        System.out.println("Subjects:");
        for(String sub : subjects) {
            System.out.println("\t" + sub);
        }
        System.out.println("Personal-Number  Class");
        for(int i = 0; i < students.size(); i++) {
            System.out.println(students.get(i).getPersonalNumber() + " \t " + students.get(i).getClassOfStudent());
        }
    }
}
