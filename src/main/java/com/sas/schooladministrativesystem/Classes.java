package com.sas.schooladministrativesystem;

/**
 *
 * @author Manas
 */
public class Classes {
    final private int maxStudentsInClass = 20;
    
    //classAplabets is for setting classes 1A, 1B etc.
    final private String[] classAplabets = {"A", "B", "C", "D", "E", "F", "G", "H", "I"};
   
    //sets the class of the student within the grade. Based on first come first serve.
    public String setClass(int grade, int totalNoOfStudents, int indexOfStudentInQueue) {
        if (totalNoOfStudents > maxStudentsInClass) {
            if(indexOfStudentInQueue % maxStudentsInClass == 0) {
                return String.valueOf(grade) + classAplabets[(indexOfStudentInQueue/maxStudentsInClass) - 1];
            }
            else {
                return String.valueOf(grade) + classAplabets[indexOfStudentInQueue/maxStudentsInClass];
            }
        }
        else 
            return String.valueOf(grade);
    }
}
