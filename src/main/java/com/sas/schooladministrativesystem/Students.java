package com.sas.schooladministrativesystem;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 *
 * @author Manas
 */
public class Students {
    private String personalNumber = null;
    private String name = null;
    private String address = null;
    private int grade = 0;
    private String[] individualSubjects = null;
    private String classOfStudent = null;
    
    Students(String personalNumber, String name, String address, int grade, String[] individualSubjects) {
        this.personalNumber = personalNumber;
        this.name = name;
        this.address = address;
        this.grade = grade;
        this.individualSubjects = individualSubjects;
    }
    
    //gets complete subjects that the student is studying in the school.
    public String[] getCompleteSubjects() {
        Subjects subjects = new Subjects();
        String[] mandatorySubjects = subjects.getCommonSubjects(grade);
        
        Set<String> tempList = new LinkedHashSet<String>();
        tempList.addAll(Arrays.asList(mandatorySubjects));
        tempList.addAll(Arrays.asList(individualSubjects));
        String tempArray[] = tempList.toArray(new String[tempList.size()]);
        
        return tempArray;
    }
    
    //gets the individual subjects that the student is studying in the school.
    public String[] getIndividualStubjects() {
        return individualSubjects;
    }
    
    //sets the class of the student in the grade level.
    public void setClassOfStudent(String classOfStudent) {
        this.classOfStudent = classOfStudent;
    }
    
    //gets the class of the student in the grade level.
    public String getClassOfStudent() {
        return classOfStudent;
    }  
    
    //gets the grade of the student.
    public int getGradeOfStudent() {
        return grade;
    }
    
    //gets the personal number of the student.
    public String getPersonalNumber() {
        return personalNumber;
    }
}
