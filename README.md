# School Administrative System
## Description
Students of a school Are divided into classes, usually one per grade level. Each student studies a set of subjects, some common (e.g math, geography) and some individual chosen (e.g.
Swedish, French). Students are then organized into groups where teachers hold lessons in those subjects. A group can contain more than one subject (e.g physics and chemistry).

## Implementation
* Data model of students, classes, subjects and groups were implemented.
* Categorization of students into groups based on the subjects was implemented.
* Test data is in testData.json file.
* testData.json file contain 100 records.

## Execution
* Execute TestRunner.java to observe the results.